package pl.ppteam.doctord.queue.configuration

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableSharedFlow
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.ppteam.doctord.queue.domain.QueueRecord

@Configuration
class QueueConfiguration {

    @Bean
    fun queueUpdateFlow(): MutableSharedFlow<QueueRecord> = MutableSharedFlow(0)

    @Bean
    fun queueCoroutineScope() = CoroutineScope(SupervisorJob())

}