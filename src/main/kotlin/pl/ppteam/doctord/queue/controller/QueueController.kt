package pl.ppteam.doctord.queue.controller

import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.context.ApplicationEventPublisher
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.queue.domain.QueueEvent
import pl.ppteam.doctord.queue.repository.QueueRepository
import pl.ppteam.doctord.queue.service.QueueService
import pl.ppteam.doctord.security.getCurrentUserID

@RestController
@RequestMapping("/api/v1/queue")
class QueueController(
    private val service: QueueService,
    private val repo: QueueRepository,
    private val evtPub: ApplicationEventPublisher,
) {

    @GetMapping(produces = [MediaType.APPLICATION_STREAM_JSON_VALUE])
    fun getQueueRecordOfCurrentUser() = service.queueFlow(getCurrentUserID())

    @GetMapping("/all", produces = [MediaType.APPLICATION_STREAM_JSON_VALUE])
    fun getAllQueueRecords() = service.queueFlow()

    @DeleteMapping("/{recordId}")
    suspend fun deleteRecord(@PathVariable recordId: ID) = repo.findById(recordId).awaitFirstOrNull()?.let { record ->
        repo.delete(record)
            .subscribe {
                evtPub.publishEvent(QueueEvent.Deleted(record))
            }
    }
}