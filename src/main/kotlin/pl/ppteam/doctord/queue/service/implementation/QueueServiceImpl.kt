package pl.ppteam.doctord.queue.service.implementation

import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flattenMerge
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrDefault
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.reactive.awaitSingleOrNull
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.Appointment
import pl.ppteam.doctord.queue.domain.QueueEvent
import pl.ppteam.doctord.queue.domain.QueueRecord
import pl.ppteam.doctord.queue.repository.QueueRepository
import pl.ppteam.doctord.queue.service.QueueService
import pl.ppteam.doctord.util.info
import java.time.Instant

@FlowPreview
@Service
class QueueServiceImplt(
    private val queueRepository: QueueRepository,
    private val queueEventPublisher: ApplicationEventPublisher,
    private val watchFlow: Flow<QueueRecord>,
) : QueueService {

    override suspend fun appendAppointment(appointment: Appointment) {
        val queueNumber = findQueueNumber()

        val record = QueueRecord(appointment.id!!, appointment.userID, queueNumber)
            .saveAndAwait()

        info("Appended event to queue. $record")
        queueEventPublisher.publishEvent(QueueEvent.Created(record))
    }

    override fun queueFlow(userID: ID) = flowOf(
        queueRepository.findAllByUserID(userID).asFlow(),
        watchFlow.filter { it.userID == userID }
    )
        .flattenMerge()

    override fun queueFlow() = flowOf(
        queueRepository.findAllByValidUntilAfterOrderByNumberAsc(Instant.now()).asFlow(),
        watchFlow
    )
        .flattenMerge()

    override suspend fun getFirstFromQueue() = queueRepository
        .findFirstByAppointmentIDNotNullOrderByNumberAsc()
        .awaitFirstOrNull()

    override suspend fun getRecordById(recordId: ID): QueueRecord? = queueRepository
        .findById(recordId)
        .awaitSingleOrNull()

    private suspend fun findQueueNumber() = queueRepository
        .findFirstByAppointmentIDNotNullOrderByNumberDesc()
        .map { it.number }
        .awaitFirstOrDefault(-1)
        .inc()

    private suspend fun QueueRecord.saveAndAwait() = queueRepository
        .save(this)
        .awaitSingle()

}