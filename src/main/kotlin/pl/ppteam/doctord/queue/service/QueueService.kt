package pl.ppteam.doctord.queue.service

import kotlinx.coroutines.flow.Flow
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.Appointment
import pl.ppteam.doctord.queue.domain.QueueRecord

interface QueueService {

    suspend fun appendAppointment(appointment: Appointment)

    suspend fun getFirstFromQueue(): QueueRecord?

    suspend fun getRecordById(recordId: ID): QueueRecord?

    fun queueFlow(userID: ID): Flow<QueueRecord>

    fun queueFlow(): Flow<QueueRecord>

}

