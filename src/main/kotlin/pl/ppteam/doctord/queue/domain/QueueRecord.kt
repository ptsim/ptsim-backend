package pl.ppteam.doctord.queue.domain

import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.connection.domain.ConnectionOffer
import java.time.Instant

@Document("QueueRecord")
data class QueueRecord(

    @Indexed(unique = true)
    val appointmentID: ID,

    @Indexed(unique = true, expireAfterSeconds = ConnectionOffer.OFFER_VALIDITY_SECONDS)
    val userID: ID,

    @Indexed(unique = true)
    val number: Long,

    var id: ID? = null,

    val validUntil: Instant = Instant.now().plusSeconds(ConnectionOffer.OFFER_VALIDITY_SECONDS.toLong()),
)