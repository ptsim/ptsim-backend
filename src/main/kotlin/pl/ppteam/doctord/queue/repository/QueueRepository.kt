package pl.ppteam.doctord.queue.repository

import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.queue.domain.QueueRecord
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant

interface QueueRepository : ReactiveMongoRepository<QueueRecord, ID> {

    fun findAllByUserID(id: String): Flux<QueueRecord>

    fun findFirstByAppointmentIDNotNullOrderByNumberDesc(): Mono<QueueRecord>

    fun findFirstByAppointmentIDNotNullOrderByNumberAsc(): Mono<QueueRecord>

    fun findAllByValidUntilAfterOrderByNumberAsc(after: Instant): Flux<QueueRecord>

}