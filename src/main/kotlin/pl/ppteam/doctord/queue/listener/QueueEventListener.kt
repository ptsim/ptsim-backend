package pl.ppteam.doctord.queue.listener

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Update
import org.springframework.data.mongodb.core.query.where
import org.springframework.data.mongodb.core.update
import org.springframework.stereotype.Component
import pl.ppteam.doctord.queue.domain.QueueEvent
import pl.ppteam.doctord.queue.domain.QueueRecord
import pl.ppteam.doctord.queue.repository.QueueRepository
import pl.ppteam.doctord.util.info

@Component
class QueueEventListener(
    private val queueUpdateFlow: MutableSharedFlow<QueueRecord>,
    private val queueCoroutineScope: CoroutineScope,
    private val queueRepository: QueueRepository,
    private val mongoTemplate: ReactiveMongoTemplate,
    private val eventPublisher: ApplicationEventPublisher,
) {

    @EventListener
    fun queueRecordCreated(evt: QueueEvent.Created) = queueCoroutineScope.launch {
        info("Handling QueueEvent.Created $evt")
        queueUpdateFlow.emit(evt.record)
    }

    @EventListener
    fun queueRecordDeleted(evt: QueueEvent.Deleted) = queueCoroutineScope.launch {
        val record = evt.record

        info("Handling QueueEvent.Deleted $evt")
        mongoTemplate.update<QueueRecord>()
            .matching(where(QueueRecord::number).gt(record.number))
            .apply(Update().inc("number", -1))
            .all()
            .awaitFirstOrNull()

        queueUpdateFlow.emitAll(queueRepository.findAll().asFlow())

        eventPublisher.publishEvent(QueueEvent.Moved)
    }

}