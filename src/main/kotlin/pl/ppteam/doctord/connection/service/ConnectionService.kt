package pl.ppteam.doctord.connection.service

import kotlinx.coroutines.flow.Flow
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.connection.controller.RoomCreateRequest
import pl.ppteam.doctord.connection.domain.ConnectionOffer
import pl.ppteam.doctord.connection.domain.OpenviduTokenResponse

interface ConnectionService {

    suspend fun offerConnection(offer: ConnectionOffer): ConnectionOffer?

    fun connectionOfferFlow(userID: ID): Flow<ConnectionOffer>

    suspend fun acceptConnection(offerID: ID, currentUserID: ID): OpenviduTokenResponse?

    fun createRoom(roomCreateRequest: RoomCreateRequest, currentUserID: ID): OpenviduTokenResponse

    fun deleteRoom(roomCode: String, currentUserID: ID)
}