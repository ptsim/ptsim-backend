package pl.ppteam.doctord.connection.service.implementation

import io.openvidu.java.client.ConnectionProperties
import io.openvidu.java.client.ConnectionType
import io.openvidu.java.client.OpenVidu
import io.openvidu.java.client.OpenViduHttpException
import io.openvidu.java.client.OpenViduRole
import org.springframework.stereotype.Service
import pl.ppteam.doctord.connection.domain.ConnectionOffer
import pl.ppteam.doctord.connection.domain.OpenviduTokenResponse
import pl.ppteam.doctord.connection.repository.OpenViduSessionTokenRepository

@Service
class OpenViduService(
    private val openVidu: OpenVidu,
    private val sessionRepository: OpenViduSessionTokenRepository,
) {

    fun join(offer: ConnectionOffer): OpenviduTokenResponse? =
        sessionRepository.getSessionByName(offer.code)?.let { session ->
            val props = ConnectionProperties.Builder()
                .type(ConnectionType.WEBRTC)
                .role(OpenViduRole.PUBLISHER)
                .build()

            try {
                val connection = session.createConnection(props)
                sessionRepository.storeConnection(offer.code, connection)

                OpenviduTokenResponse(connection.token)
            } catch (e: OpenViduHttpException) {
                sessionRepository.deleteSession(offer.code)

                null
            }
        }

    fun createRoom(code: String): OpenviduTokenResponse {
        val session = openVidu.createSession()
        val props = ConnectionProperties.Builder()
            .type(ConnectionType.WEBRTC)
            .role(OpenViduRole.MODERATOR)
            .build()

        val connection = session.createConnection(props)

        sessionRepository.storeNewSession(code, session, connection)
        return OpenviduTokenResponse(connection.token)
    }

    fun deleteRoom(roomCode: String) = sessionRepository.deleteSession(roomCode)
}