package pl.ppteam.doctord.connection.service.implementation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.filter
import org.springframework.stereotype.Service
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.connection.controller.RoomCreateRequest
import pl.ppteam.doctord.connection.domain.ConnectionOffer
import pl.ppteam.doctord.connection.domain.OpenviduTokenResponse
import pl.ppteam.doctord.connection.repository.ConnectionOfferRepository
import pl.ppteam.doctord.connection.service.ConnectionService
import pl.ppteam.doctord.util.info

@Service
class ConnectionServiceImpl(
    private val connectionFlow: MutableSharedFlow<ConnectionOffer>,
    private val connectionOfferRepository: ConnectionOfferRepository,
    private val openViduService: OpenViduService,
) : ConnectionService {

    override suspend fun offerConnection(offer: ConnectionOffer): ConnectionOffer? =
        connectionOfferRepository
            .save(offer)
            .let { savedOffer ->
                connectionFlow.emit(savedOffer)
                info("Offer emitted to flow. $offer")
                savedOffer
            }

    override fun connectionOfferFlow(userID: ID): Flow<ConnectionOffer> =
        connectionFlow
            .filter { it.userID == userID }

    override suspend fun acceptConnection(offerID: ID, currentUserID: ID): OpenviduTokenResponse? =
        connectionOfferRepository
            .info("User $currentUserID trying to accept offer: $offerID")
            .findByID(offerID)
            .info { "Found offer: $it" }
            ?.let {
                if (it.userID == currentUserID) {
                    openViduService.join(it)
                } else {
                    null
                }
            }

    override fun createRoom(roomCreateRequest: RoomCreateRequest, currentUserID: ID) =
        openViduService
            .createRoom(roomCreateRequest.code)

    // TODO: secure -> allow removing only currentUser's (doctor) session
    override fun deleteRoom(roomCode: String, currentUserID: ID) =
        openViduService
            .deleteRoom(roomCode)
}
