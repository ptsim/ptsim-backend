package pl.ppteam.doctord.connection.configuration

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableSharedFlow
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.ppteam.doctord.connection.domain.ConnectionOffer

@Configuration
class ConnectionConfiguration {

    @Bean
    fun connectionFlow() = MutableSharedFlow<ConnectionOffer>(20)

    @Bean
    fun connectionScope() = CoroutineScope(SupervisorJob())

}