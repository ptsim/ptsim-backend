package pl.ppteam.doctord.connection.configuration

import io.openvidu.java.client.OpenVidu
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenViduConfiguration {

    @Value("\${openvidu.url}")
    private lateinit var openviduUrl: String

    @Value("\${openvidu.secret}")
    private lateinit var openviduSecret: String

    @Bean
    fun openVidu() = OpenVidu(openviduUrl, openviduSecret)

}