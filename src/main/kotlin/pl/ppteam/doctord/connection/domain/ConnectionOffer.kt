package pl.ppteam.doctord.connection.domain

import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.util.randomID
import java.time.Instant

@Document("connection_offers")
data class ConnectionOffer(
    val code: String,
    @Indexed(expireAfterSeconds = OFFER_VALIDITY_SECONDS)
    val userID: ID,
    val validUntil: Instant = Instant.now().plusSeconds(OFFER_VALIDITY_SECONDS.toLong()),
    var id: String = randomID(),
) {

    companion object {
        const val OFFER_VALIDITY_SECONDS = 5 * 60
    }
}
