package pl.ppteam.doctord.connection.domain

data class OpenviduTokenResponse(
    val token: String?,
)