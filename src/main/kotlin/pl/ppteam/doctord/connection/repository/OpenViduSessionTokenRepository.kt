package pl.ppteam.doctord.connection.repository

import io.openvidu.java.client.Connection
import io.openvidu.java.client.OpenViduRole
import io.openvidu.java.client.Session
import org.springframework.stereotype.Repository
import java.util.concurrent.ConcurrentHashMap

@Repository
class OpenViduSessionTokenRepository {

    private val sessions = ConcurrentHashMap<String, Session>()
    private val sessionNamesTokens = ConcurrentHashMap<String, ConcurrentHashMap<String, OpenViduRole>>()

    fun storeNewSession(sessionName: String, session: Session, connection: Connection) {
        sessions[sessionName] = session
        sessionNamesTokens[sessionName] = ConcurrentHashMap(mutableMapOf(connection.token to connection.role))
    }

    fun storeConnection(sessionName: String, connection: Connection) {
        sessionNamesTokens[sessionName]?.put(connection.token, connection.role)
    }

    fun getSessionByName(sessionName: String): Session? = sessions[sessionName]

    fun deleteSession(sessionName: String) {
        sessions.remove(sessionName)
        sessionNamesTokens.remove(sessionName)
    }

}