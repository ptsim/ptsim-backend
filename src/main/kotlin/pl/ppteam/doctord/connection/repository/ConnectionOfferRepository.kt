package pl.ppteam.doctord.connection.repository

import org.springframework.stereotype.Repository
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.connection.domain.ConnectionOffer
import java.util.concurrent.ConcurrentHashMap

@Repository
class ConnectionOfferRepository {

    private val offers = ConcurrentHashMap<String, ConnectionOffer>()

    fun save(offer: ConnectionOffer): ConnectionOffer {
        offers[offer.id] = offer
        return offer
    }

    fun findByID(offerID: ID) =
        offers[offerID]
}