package pl.ppteam.doctord.connection.controller

import kotlinx.coroutines.flow.Flow
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.connection.domain.ConnectionOffer
import pl.ppteam.doctord.connection.service.ConnectionService
import pl.ppteam.doctord.security.getCurrentUserID

@RestController
@RequestMapping("/api/v1/connections")
class ConnectionController(
    private val connectionService: ConnectionService,
) {

    @GetMapping(produces = [MediaType.APPLICATION_STREAM_JSON_VALUE])
    fun listenForConnection(): Flow<ConnectionOffer> =
        connectionService
            .connectionOfferFlow(getCurrentUserID())

    @GetMapping("/offers/{offerID}")
    suspend fun acceptConnection(@PathVariable offerID: ID) =
        connectionService
            .acceptConnection(offerID, getCurrentUserID())

    @PostMapping("/offers")
    //@Secured(DOCTOR)
    suspend fun offerConnection(@RequestBody offer: ConnectionOffer) =
        connectionService
            .offerConnection(offer)

    @PostMapping("/rooms")
    //@Secured(DOCTOR)
    suspend fun createRoom(@RequestBody roomCreateRequest: RoomCreateRequest) =
        connectionService
            .createRoom(roomCreateRequest, getCurrentUserID())

    @DeleteMapping("/rooms/{code}")
    //@Secured(DOCTOR)
    suspend fun deleteRoom(@PathVariable("code") roomCode: String) =
        connectionService
            .deleteRoom(roomCode, getCurrentUserID())
}

data class RoomCreateRequest(
    val code: String,
)