package pl.ppteam.doctord.util

import org.slf4j.LoggerFactory

val log = LoggerFactory.getLogger("SimpleGlobalLogger")

fun <T> T.info(msg: String): T = this.run {
    log.info(msg)
    this
}

fun <T> T.info(msg: (T) -> String): T = this.run {
    log.info(msg(this))
    this
}

fun info(msg: String) = log.info(msg)