package pl.ppteam.doctord.security

import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.util.info
import reactor.core.publisher.Mono

private var currentUserID = "developmentUserID"

fun getCurrentUserID(): ID = currentUserID

@Component
class TestAuthenticator : WebFilter {

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> =
        chain.filter(exchange)
            .doFirst {
                val usrId = exchange.request.headers
                    .getFirst("Authorization")
                    ?.removePrefix("Bearer ")
                    ?: "notLoggedIntUser"
                info("Logging in as userID: $usrId")
                currentUserID = usrId
            }

}
