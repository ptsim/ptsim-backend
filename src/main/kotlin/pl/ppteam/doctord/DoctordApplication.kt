package pl.ppteam.doctord

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.data.mapping.context.MappingContext
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.index.IndexDefinition
import org.springframework.data.mongodb.core.index.IndexResolver
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver
import org.springframework.data.mongodb.core.index.ReactiveIndexOperations
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories
import org.springframework.stereotype.Component

@SpringBootApplication
@EnableReactiveMongoRepositories
class DoctordApplication

fun main(args: Array<String>) {
    runApplication<DoctordApplication>(*args)
}

@Component
// TODO: not initialziing properly
class IndexMongoCreator(
    private val mongoTemplate: ReactiveMongoTemplate,
) {

    @EventListener(ContextRefreshedEvent::class)
    fun initIndicesAfterStartup() {
        val mappingContext: MappingContext<out MongoPersistentEntity<*>?, MongoPersistentProperty> = mongoTemplate
            .converter.mappingContext

        val resolver: IndexResolver = MongoPersistentEntityIndexResolver(mappingContext)
        mappingContext.persistentEntities
            .filter { it!!.isAnnotationPresent(Document::class.java) }
            .forEach {
                val indexOps: ReactiveIndexOperations = mongoTemplate.indexOps(it!!.type)
                resolver.resolveIndexFor(it.type)
                    .forEach { indexDefinition: IndexDefinition? -> indexOps.ensureIndex(indexDefinition!!) }
            }
    }
}