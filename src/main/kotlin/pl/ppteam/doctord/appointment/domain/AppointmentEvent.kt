package pl.ppteam.doctord.appointment.domain

import pl.ppteam.doctord.appointment.Appointment

sealed class AppointmentEvent {

    data class Created(val appointment: Appointment) : AppointmentEvent()
}