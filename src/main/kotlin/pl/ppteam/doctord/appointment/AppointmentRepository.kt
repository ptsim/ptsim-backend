package pl.ppteam.doctord.appointment

import kotlinx.coroutines.flow.Flow
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import pl.ppteam.doctord.ID
import reactor.core.publisher.Mono

interface AppointmentRepository : ReactiveMongoRepository<Appointment, ID> {

    fun findAllByIsHandledFalseAndIsExpiredFalseOrderByCreatedAtAsc(): Flow<Appointment>

    fun findByUserID(userID: ID): Mono<Appointment>
}