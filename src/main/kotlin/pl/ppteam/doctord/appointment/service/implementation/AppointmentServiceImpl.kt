package pl.ppteam.doctord.appointment.service.implementation

import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.Appointment
import pl.ppteam.doctord.appointment.AppointmentCreateRequest
import pl.ppteam.doctord.appointment.AppointmentRepository
import pl.ppteam.doctord.appointment.domain.AppointmentEvent
import pl.ppteam.doctord.appointment.service.AppointmentService
import pl.ppteam.doctord.appointment.toDomain
import pl.ppteam.doctord.security.getCurrentUserID
import pl.ppteam.doctord.util.info

@Service
class AppointmentServiceImpl(
    private val appointmentRepository: AppointmentRepository,
    private val eventPublisher: ApplicationEventPublisher,
) : AppointmentService {

    override suspend fun getCurrentUsersActiveAppointment(userID: ID): Appointment {
        TODO("Not yet implemented")
    }

    override suspend fun updateAppointmentDescription(request: AppointmentCreateRequest, id: ID): Appointment {
        TODO("Not yet implemented")
    }

    override suspend fun createOrGetActiveAppointment(request: AppointmentCreateRequest): Appointment {
        val appointment = appointmentRepository
            .findByUserID(getCurrentUserID())
            .awaitFirstOrNull()
            ?.let { return it.info("Found existing appointment. Not creating new.") }
            ?: appointmentRepository.save(request.toDomain(getCurrentUserID()))
                .awaitSingle()

        eventPublisher.publishEvent(AppointmentEvent.Created(appointment))
            .info("Appointment created. Published appointment event $appointment")

        return appointment
    }

    override suspend fun deleteAppointment(id: ID) =
        appointmentRepository.deleteById(id).awaitFirst().run { Unit }

    override suspend fun getAppointmentById(id: ID): Appointment? =
        appointmentRepository.findById(id).awaitFirstOrNull()

}
