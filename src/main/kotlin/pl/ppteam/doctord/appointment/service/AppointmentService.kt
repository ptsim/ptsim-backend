package pl.ppteam.doctord.appointment.service

import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.Appointment
import pl.ppteam.doctord.appointment.AppointmentCreateRequest

interface AppointmentService {

    suspend fun getCurrentUsersActiveAppointment(userID: ID): Appointment
    suspend fun updateAppointmentDescription(request: AppointmentCreateRequest, id: ID): Appointment
    suspend fun createOrGetActiveAppointment(request: AppointmentCreateRequest): Appointment
    suspend fun deleteAppointment(id: ID)
    suspend fun getAppointmentById(id: ID): Appointment?

}

