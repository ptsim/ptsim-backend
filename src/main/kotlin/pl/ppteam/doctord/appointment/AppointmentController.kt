package pl.ppteam.doctord.appointment

import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.service.AppointmentService
import pl.ppteam.doctord.security.getCurrentUserID

@RestController
@RequestMapping("/api/v1/appointments")
class AppointmentController(
    private val appointmentService: AppointmentService,
) {

    @GetMapping
    suspend fun getCurrentUsersActiveAppointment(): Appointment? =
        appointmentService.getCurrentUsersActiveAppointment(getCurrentUserID())

    @PostMapping
    suspend fun createOrGetActiveAppointment(
        @RequestBody request: AppointmentCreateRequest,
    ): Appointment =
        appointmentService.createOrGetActiveAppointment(request)

    @GetMapping("/{id}")
    suspend fun getAppointmentById(@PathVariable id: ID) =
        appointmentService.getAppointmentById(id)

    @PutMapping("/{id}")
    suspend fun updateAppointment(
        @PathVariable id: String,
        @RequestBody request: AppointmentCreateRequest,
    ): Appointment =
        appointmentService.updateAppointmentDescription(request, id)

    @DeleteMapping("/{id}")
    suspend fun deleteAppointment(@PathVariable id: ID) =
        appointmentService.deleteAppointment(id)

}

