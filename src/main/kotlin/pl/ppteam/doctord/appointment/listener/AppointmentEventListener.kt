package pl.ppteam.doctord.appointment.listener

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import pl.ppteam.doctord.appointment.domain.AppointmentEvent
import pl.ppteam.doctord.queue.service.QueueService
import pl.ppteam.doctord.util.info

@Component
class AppointmentEventListener(
    private val appointmentCreatedEventScope: CoroutineScope,
    private val queueService: QueueService,
) {

    @EventListener
    fun handleAppointmentCreated(evt: AppointmentEvent.Created) = appointmentCreatedEventScope.launch {
        info("Handling AppointmentEvent.Created event: $evt")
        queueService.appendAppointment(evt.appointment)
    }
}