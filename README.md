# Tryb testowy

Tryb testowy ma wyłączoną autentykację. Autoryzacja odbywa się za pomocą nagłówka `Authorization` poprzez podanie
jako wartość id użytkownika, którego chcemy użyć, np.:

`Authorization: user123512` 

lub

`Authorization: Bearer user123512`

spowoduje ustawienie wartości `user123512` jako id użytkownika w kontekście aplikacji (w przyp. drugiego wariantu prefix `Bearer ` zostanie utomatycznie usunięty). Pobranie obecnego id użytkownika odbywa się poprzez wywołanie funkcji `getCurrentUserId()`.

**Uwaga: brak zabezpieczenia wątków! W trybie testowym należy wykonywać zapytania HTTP sekwencyjnie.**

# API

Interaktywna dokumentacja API 'Swagger' jest domyślnie dostępna pod adresem: `localhost:8080/swagger-ui/`

# Przykładowe flow

1. Lekarz tworzy sesję z kodem `someCodeUsedForJoining`. Kod ten posłuży do dołączania do pokoju lekarza.
   
    `curl -X POST "http://localhost:8080/api/v1/connections/rooms" -H  "Authorization: Bearer doctorID" -H  "Content-Type: application/json" -d "{\"code\":\"someCodeUsedForJoining\"}"`

    W odpowiedzi otrzyma token sesji OpenVidu, którym może się połączyć z serwerem RTC.

2. Lekarz nasłuchuje całą kolejkę i czeka na zgłoszenia:

    `curl -X GET "http://localhost:8080/api/v1/queue/all" -H  "Authorization: Bearer doctorID"`

3. Pacjent umawia się na wizytę podając opis dolegliwości.

    `curl -X POST "http://localhost:8080/api/v1/appointments" -H  "Authorization: Bearer pacjentID_1" -H  "Content-Type: application/json" -d "{\"description\":\"Opis dolegliwości\"}"`

    Po poprawnym dodaniu zostaje automatycznie zakolejkowany.

4. Pacjent obserwuje kolejkę. Zostają zwrócone tylko rekordy dotyczące jego.

     `curl -X GET "http://localhost:8080/api/v1/queue" -H  "Authorization: Bearer pacjentID_1"`

5. Jednocześnie pacjent nasłuchuje oferty połączenia:

    `curl -X GET "http://localhost:8080/api/v1/connections" -H  "Authorization: Bearer pacjentID_1"`

6. Lekarz otrzymuje ten sam rekord w kolejce. Tworzy więc ofertę połączenia:

    `curl -X POST "http://localhost:8080/api/v1/connections/offers" -H  "Authorization: Bearer doctorID" -H  "Content-Type: application/json" -d "{\"code\":\"someCodeUsedForJoining\",\"userID\":\"pacjentID_1\"}"`

    Oferta domyślnie jest ważna 5 minut.

7. Pacjent otrzymuje ofertę połączenia. Może ją zaakceptować:

    `curl -X GET "http://localhost:8080/api/v1/connections/offers/offerID123" -H  "Authorization: Bearer pacjentID_1"`

    W odpowiedzi otrzymuje token OpenVidu lub pustą odpowiedź jesli oferta wygasła lub wykonano nieprawidłową akcję.